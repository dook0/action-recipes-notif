#!/usr/bin/env python2
# -*-: coding utf-8 -*-

from hermes_python.hermes import Hermes
from snipshelpers.thread_handler import ThreadHandler
from hermes_python.ontology import *
import os
import time
from apns import APNs, Frame, Payload
import unicodedata


import Queue
import requests
import urllib2
import json

CONFIGURATION_ENCODING_FORMAT = "utf-8"
CONFIG_INI = "config.ini"

MQTT_IP_ADDR = "localhost"
MQTT_PORT = 1883
MQTT_ADDR = "{}:{}".format(MQTT_IP_ADDR, str(MQTT_PORT))

apns = APNs(use_sandbox=True, cert_file='aps_dev_cert.pem', key_file='aps_dev_key_decrypted.pem')


class Skill:

    def __init__(self):
        self.queue = Queue.Queue()
        self.thread_handler = ThreadHandler()
        self.thread_handler.run(target=self.start_blocking)
        self.thread_handler.start_run_loop()

    def start_blocking(self, run_event):
        while run_event.is_set():
            try:
                self.queue.get(False)
            except Queue.Empty:
                with Hermes(MQTT_ADDR) as h:
                    h.subscribe_intents(self.callback).start()

    def extract_dishs(self, intent_message):
        dishs = []
        if intent_message.slots.dish is not None:
            for adish in intent_message.slots.dish:
                dishs.append(adish.slot_value.value.value)
        return dishs

    def callback(self, hermes, intent_message):

        dishs = self.extract_dishs(intent_message)

        if intent_message.intent.intent_name == 'Dook:getRecipe':
            self.queue.put(self.appliance_get_recipe(hermes, intent_message, dishs))

    def appliance_get_recipe(self, hermes, intent_message, dishs):

        # Send a notification
        token_hex = 'f9c602a5a7cc40bad791113544d423eee622304367af8bee7a5c1e39fafe1458'
        # output = unicodedata.normalize('NFD', dishs[0]).encode('ascii', 'ignore')
        s = dishs[0]
        s = s.decode('latin-1')
        s = s.encode('utf-8')
        s = urllib2.quote(s)
        payload = Payload(alert="la recete du "+ dishs[0].decode("latin-1"), sound="default", badge=1,custom={'toLoad':'http://www.marmiton.org/recettes/recherche.aspx?type=all&aqt='+s})
        apns.gateway_server.send_notification(token_hex, payload)
        result_sentence="C'est envoy\xc3\xa9 sur votre t\xc3\xa9l\xc3\xa9phone"
        hermes.publish_end_session(intent_message.session_id, result_sentence)

if __name__ == "__main__":
    Skill()
