import time
from apns import APNs, Frame, Payload

apns = APNs(use_sandbox=True, cert_file='aps_dev_cert.pem', key_file='aps_dev_key_decrypted.pem')

# Send a notification
token_hex = 'f9c602a5a7cc40bad791113544d423eee622304367af8bee7a5c1e39fafe1458'
payload = Payload(alert="Hello World!", sound="default", badge=1,custom={'toLoad':'http://www.marmiton.org/recettes/recherche.aspx?type=all&aqt=salade-de-fruit'})
apns.gateway_server.send_notification(token_hex, payload)
